﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NexTime_Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace NexTime_Controller.Tests
{
    [TestClass()]
    public class RowComparerTests
    {

        [TestMethod()]
        public void GetMissingRowsTest_AllRowsAreFound_ReturnEmptyMissingRows()
        {
            var rowComparer = new RowComparer(new List<Row> { new Row("1", 1), new Row("asd", 10), new Row("asd3", 10) },
                                      new List<Row> { new NextimeRow("1", 1, 10), new NextimeRow("asd", 10, 5), }, "symbol");

            var result = rowComparer.GetMissingRows();
            Assert.AreEqual(0, result.Count);

        }

        [TestMethod()]
        public void GetMissingRowsTest_OneRowArentFound_ReturnOneMissingRow()
        {
            var rowComparer = new RowComparer(new List<Row> { new Row("1", 1), new Row("asd", 10), new Row("asd3", 10) },
                                      new List<Row> { new NextimeRow("1", 1, 10), new NextimeRow("asd2", 10, 5), }, "symbol");

            var result = rowComparer.GetMissingRows();
            Assert.AreEqual(1, result.Count);

        }

        [TestMethod()]
        public void GetMissingRowsTest_TwoRowsArentFound_ReturnTwoMissingRows()
        {
            var rowComparer = new RowComparer(new List<Row> { new Row("1", 1), new Row("asd", 10), new Row("asd3", 10) },
                                      new List<Row> { new NextimeRow("2", 1, 10), new NextimeRow("asd2", 10, 5), }, "symbol");

            var result = rowComparer.GetMissingRows();
            Assert.AreEqual(2, result.Count);

        }

        [TestMethod()]
        public void GetMissingRowsTest_IncorrectKey_ReturnAll()
        {
            var rowComparer = new RowComparer(new List<Row> { new Row("1", 1), new Row("asd", 10), new Row("asd3", 10) },
                                      new List<Row> { new NextimeRow("2", 1, 10), new NextimeRow("asd2", 10, 5), }, "test");

            var result = rowComparer.GetMissingRows();
            Assert.AreEqual(2, result.Count);

        }

        [TestMethod()]
        public void GetMissingRowsTest_EmptyNextimeRows_ReturnZero()
        {
            var rowComparer = new RowComparer(new List<Row> { new Row("1", 1), new Row("asd", 10), new Row("asd3", 10) },
                                      new List<Row>(), "symbol");

            var result = rowComparer.GetMissingRows();
            Assert.AreEqual(0, result.Count);

        }

        [TestMethod()]
        public void GetMissingRowsTest_EmptyVivertoRows_ReturnTwo()
        {
            var rowComparer = new RowComparer(new List<Row>(),
                                      new List<Row> { new NextimeRow("2", 1, 10), new NextimeRow("asd2", 10, 5), }, "symbol");

            var result = rowComparer.GetMissingRows();
            Assert.AreEqual(2, result.Count);

        }
    }
}