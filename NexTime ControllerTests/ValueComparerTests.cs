﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NexTime_Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexTime_Controller.Tests
{
    [TestClass()]
    public class ValueComparerTests
    {
        [TestMethod()]
        public void ExistsTest_ValueExists_ReturnTrue()
        {
            //AAA
            ValueComparer valueComparer = new ValueComparer();

            List<string> target = new List<string> { "tes", "test", "haha" };
            string value = "test";

            bool result;

            //AAA
            result = valueComparer.Exists(value, target);

            //AAA
            Assert.AreEqual(true, result);
        }

        [TestMethod()]
        public void ExistsTest_ValueNotExists_ReturnFalse()
        {
            //AAA
            ValueComparer valueComparer = new ValueComparer();

            List<string> target = new List<string> { "tes", "testt", "haha" };
            string value = "test";

            bool result;

            //AAA
            result = valueComparer.Exists(value, target);

            //AAA
            Assert.AreEqual(false, result);
        }

        [TestMethod()]
        public void ExistsTest_TargetIsNull_ReturnFalse()
        {
            //AAA
            ValueComparer valueComparer = new ValueComparer();

            List<string> target = new List<string>();
            string value = "test";

            bool result;

            //AAA
            result = valueComparer.Exists(value, target);

            //AAA
            Assert.AreEqual(false, result);
        }

        [TestMethod()]
        public void ExistsTest_ValueIsEmpty_ReturnFalse()
        {
            //AAA
            ValueComparer valueComparer = new ValueComparer();

            List<string> target = new List<string> { "tes", "testt", "haha" };
            String value = "";

            bool result;

            //AAA
            result = valueComparer.Exists(value, target);

            //AAA
            Assert.AreEqual(false, result);
        }

        [TestMethod()]
        public void ExistsTest_ValueIsNull_ReturnFalse()
        {
            //AAA
            ValueComparer valueComparer = new ValueComparer();

            List<string> target = new List<string> { "tes", "testt", "haha" };
            String value = null;

            bool result;

            //AAA
            result = valueComparer.Exists(value, target);

            //AAA
            Assert.AreEqual(false, result);
        }
    }
}