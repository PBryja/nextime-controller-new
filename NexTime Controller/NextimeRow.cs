﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexTime_Controller
{
    public class NextimeRow : Row
    {
        private int amount;

        public int Amount { get => amount; set => amount = value; }


        public NextimeRow(string symbol, int price, int amount) : base(symbol, price) 
        {
            this.amount = amount;
        }

        public override Dictionary<string, string> GetData()
        {
            Dictionary <string, string> data = base.GetData();
            data.Add("amount", amount.ToString());
            return data;
        }

        public override string ToString()
        {
            string line = base.ToString();
            line += ";" + amount.ToString();
            return line;
        }
    }
}
