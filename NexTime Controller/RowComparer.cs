﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexTime_Controller
{
    public class RowComparer
    {
        private List<Row> source;
        private List<Row> target;
        private string key;

        public RowComparer(List<Row> target, List<Row> source, string key)
        {
            this.source = source;
            this.target = target;
            this.key = key;
        }

        private string GetValueByKey(Row row)
        {
            Dictionary<string, string> dataRow = row.GetData();
            if (!dataRow.ContainsKey(key))
                return null;

            return dataRow[key];
        }

        public bool Exists(Row row, List<Row> target)
        {
           
            string value = GetValueByKey(row);

            if (value == null)
                return false;

            foreach (var itemTarget in target)
            {
                if (value.Equals(GetValueByKey(itemTarget)))
                    return true;
            }

            return false;
        }

        public List<Row> GetMissingRows()
        {
            List<Row> missingRows = new List<Row>();

            foreach (Row sought in source)
            {
                if (!Exists(sought, target))
                    missingRows.Add(sought);           
            }

            return missingRows;
        }

        public List<Row> Collection { get => source; set => source = value; }
        public List<Row> Soughts { get => target; set => target = value; }
        public string Key { get => key; set => key = value; }
    }
}
