﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexTime_Controller
{
    public class ValueComparer
    {

        public bool Exists(string value, List<string> target)
        {
            if (value == null)
                return false;

            foreach (var itemTarget in target)
            {
                if (value.Equals(itemTarget))
                    return true;
            }

            return false;
        }

        public List<string> GetMissingRows(List<string> source, List<string> target)
        {
            List<string> result = new List<string>();

            foreach (var itemSource in source)
            {
                if (!Exists(itemSource, target))
                    result.Add(itemSource);
            }
            return result;
        }
    }
}
