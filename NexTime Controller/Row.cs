﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexTime_Controller
{
    public class Row
    {
        protected int price;
        protected string symbol;

        public string Symbol { get => symbol; set => symbol = value; }
        public int Price { get => price; set => price = value; }

        public Row(string symbol, int price)
        {
            this.symbol = symbol;
            this.price = price;
        }

        public virtual Dictionary<string, string> GetData()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("price", price.ToString());
            data.Add("symbol", symbol.ToString());

            return data;
        }

        public override string ToString()
        {
            string line = symbol + ";" + price.ToString();
            return line;
        }
    }
}
